import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types'
import { Icon } from 'semantic-ui-react'

class BookersLoader extends Component {

    render() {
        if (this.props.fixed) {
            return (
                <div style={ {
                    position: 'fixed',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)'
                } }>
                    <Icon loading size={ this.props.size ? this.props.size : 'massive' } name='book' inline='centered' />
                </div >
            )
        } else {
            return (
                <div>
                    <Icon loading size={ this.props.size ? this.props.size : 'massive' } name='book' inline='centered' />
                </div >
            )
        }

    }
}

BookersLoader.propTypes = {
    size: PropTypes.string,
    fixed: PropTypes.bool
}

export default BookersLoader;