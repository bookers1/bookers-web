import { BOOKERS_TOKEN } from '../home/constants/HomeConstants'

const getAuthorization = () => ({
    Authorization: 'Bearer ' + localStorage.getItem(BOOKERS_TOKEN),
    'Content-Type': 'application/json'
})

const getClassicHeader = () => ({
    'Content-Type': 'application/json'
})

export {
    getAuthorization,
    getClassicHeader
}
