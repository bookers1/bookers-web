import {
    SEARCH_BOOKS,
    BOOKS
} from '../constants/BooksConstants'
import DtoBooks from '../dto/DtoBooks'

export const store = {
    books: [],
    userBooks: []
}

export function BooksReducer(state = {}, action) {
    switch (action.type) {
        case SEARCH_BOOKS:
            return {
                ...state,
                books: action.books.map(book => new DtoBooks(book))
            }
        case BOOKS:
            return {
                ...state,
                userBooks: action.userBooks.map(userBook => new DtoBooks(userBook))
            }
        default:
            return state
    }
}
