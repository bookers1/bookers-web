export default class {
    constructor(obj = {}) {
        this.thumbnail = obj.thumbnail
        this.smallThumbnail = obj.smallThumbnail
    }
}
