import DtoCovers from './DtoCovers'

export default class {
    constructor(obj = {}) {
        this.title = obj.title
        this.subtitle = obj.subtitle
        this.authors = obj.authors ? obj.authors : []
        this.description = obj.description
        this.selfLink = obj.selfLink
        this.covers = obj.covers ? new DtoCovers(obj.covers) : new DtoCovers()
    }
}
