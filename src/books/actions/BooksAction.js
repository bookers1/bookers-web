import ApplicationConf from '../../conf/ApplicationConf'
import { SEARCH_BOOKS, BOOKS } from '../constants/BooksConstants'
import AppStore from '../../store/AppStore'
import { getAuthorization, } from '../../utils/ActionUtils'

const BooksAction = {

    searchBooks(search, callback = () => { }) {
        fetch(ApplicationConf.books.searchBooks(search), {
            method: 'GET',
            headers: getAuthorization(),
        }).then((result) => {
            return result.json();
        }).then(json => {
            AppStore.dispatch({ type: SEARCH_BOOKS, books: json })
            callback()
        })
    },

    addBooks(idUser, bookUrl, callback = () => { }) {
        fetch(ApplicationConf.books.addBooks(), {
            method: 'POST',
            headers: getAuthorization(),
            body: JSON.stringify({
                user_id: idUser,
                books_url: bookUrl,
            })
        }).then((result) => {
            return result.json();
        }).then(json => {
            console.log(json.message)
            callback()
        })
    },

    deleteBooks(idUser, bookUrl, callback = () => { }) {
        fetch(ApplicationConf.books.deleteBooks(), {
            method: 'DELETE',
            headers: getAuthorization(),
            body: JSON.stringify({
                user_id: idUser,
                books_url: bookUrl,
            })
        }).then((result) => {
            return result.json();
        }).then(json => {
            console.log(json.message)
            callback()
        })
    },

    fetchBooks(bookUrl) {
        return fetch(ApplicationConf.books.getBooks(), {
            method: 'POST',
            headers: getAuthorization(),
            body: JSON.stringify({
                books_url: bookUrl,
            })
        }).then((result) => {
            return result.json();
        }).then(json => {
            return json
        })
    },

    loadUserBooks(bookUrls, callback = () => { }) {
        const promise = []
        bookUrls.forEach(bookUrl => {
            promise.push(BooksAction.fetchBooks(bookUrl))
        })
        Promise.all(promise).then(result => {
            AppStore.dispatch({ type: BOOKS, userBooks: result })
            callback()
        })
    },
}

export default BooksAction
