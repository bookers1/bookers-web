import React from 'react';
import Login from './home/components/Login';
import RegisterApp from './home/components/RegisterApp';
import HomeApp from './home/components/HomeApp';
import Dashboard from './home/components/Dashboard';
import { BrowserRouter, Route } from 'react-router-dom';
// import createBrowserHistory from 'history/createBrowserHistory'
import './App.css';
import UserProfile from './home/components/UserProfile';

// const history = createBrowserHistory()

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Route exact path='/' component={ HomeApp } />
        <Route path='/login' component={ Login } />
        <Route path='/register' component={ RegisterApp } />
        <Route path='/dashboard' component={ Dashboard } />
        <Route path='/userProfile' component={ UserProfile } />
      </div>
    </BrowserRouter >
  );
}

export default App;
