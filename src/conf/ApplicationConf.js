import * as APIConfig from './APIConfig'

export default {
    login: () => `${ APIConfig.API_URI }/users/authenticate`,
    register: () => `${ APIConfig.API_URI }/users/register`,
    users: {
        getAll: () => `${ APIConfig.API_URI }/users`,
        getById: (id) => `${ APIConfig.API_URI }/users/${ id }`,
    },
    books: {
        searchBooks: (search) => `${ APIConfig.API_URI }/books/${ search }`,
        getBooks: () => `${ APIConfig.API_URI }/books`,
        addBooks: () => `${ APIConfig.API_URI }/usersBooks/new`,
        deleteBooks: () => `${ APIConfig.API_URI }/usersBooks/delete`,
    },
}
