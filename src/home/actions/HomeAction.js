import ApplicationConf from '../../conf/ApplicationConf'
import { BOOKERS_TOKEN, LOGIN, USER, ACTION, REGISTRATION_SUCCESSFUL, USERNAME_ALREADY_TAKEN } from '../constants/HomeConstants'
import { headers } from '../../conf/APIConstant'
import AppStore from '../../store/AppStore'
import { toast } from 'react-toastify';
import { getAuthorization } from '../../utils/ActionUtils'

const HomeAction = {

    login(login, password, callback = () => { }) {
        fetch(ApplicationConf.login(), {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                username: login,
                password: password
            })
        }).then((result) => {
            return result.json();
        }).then(response => {
            if (response.token) {
                localStorage.setItem(BOOKERS_TOKEN, response.token)
                localStorage.setItem(LOGIN, response.id)
                AppStore.dispatch({ type: LOGIN, logged: true })
                toast.success("Vous êtes connecté !")
                callback(response)
            } else if (response.message) {
                console.log(response.message)
                callback(response)
                toast.error("Nom d'utilisateur ou mot de passe incorrect !")
            }
        }).catch(err => console.log(err))
    },

    register(user, callback = () => { }) {
        fetch(ApplicationConf.register(), {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                firstName: user.firstName,
                lastName: user.lastName,
                username: user.username,
                password: user.password
            })
        }).then((result) => {
            return result.json();
        }).then(response => {
            if (response.message === REGISTRATION_SUCCESSFUL) {
                toast.success("Compte crée !")
                callback(response)
            } else if (response.message === USERNAME_ALREADY_TAKEN) {
                toast.warning("Nom d'utilisateur deja pris !")
                callback(response)
            }
        }).catch(err => console.log(err))
    },

    logout(callback = () => { }) {
        localStorage.setItem(BOOKERS_TOKEN, '')
        localStorage.setItem(LOGIN, '')
        AppStore.dispatch({ type: LOGIN, logged: false })
        AppStore.dispatch({ type: USER, user: {} })
        callback()
        toast.info("Vous êtes deconnecté")
    },

    fetchUser(id, callback = () => { }) {
        fetch(ApplicationConf.users.getById(id), {
            method: 'GET',
            headers: getAuthorization(),
        }).then((result) => {
            return result.json();
        }).then(json => {
            AppStore.dispatch({ type: USER, user: json })
            callback()
        }).catch(err => console.log(err))
    },

    setAction(action) {
        return { type: ACTION, action: action }
    },
}

export default HomeAction
