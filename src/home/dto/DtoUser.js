export default class {
    constructor(obj = {}) {
        this.firstName = obj.firstName
        this.lastName = obj.lastName
        this.username = obj.username
        this.password = obj.password
        this.usersBooks = obj.usersBooks
    }
}
