import {
    LOGIN,
    ACTION,
    USER
} from '../constants/HomeConstants'

export const store = {
    logged: false,
    action: '',
    user: {}
}

export function HomeReducer(state = {}, action) {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                logged: action.logged
            }
        case ACTION:
            return {
                ...state,
                action: action.action
            }
        case USER:
            return {
                ...state,
                user: action.user
            }
        default:
            return state
    }
}
