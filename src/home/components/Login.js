import React from 'react';
import { Component } from 'react';
import { Input, Grid, Button } from 'semantic-ui-react'
import logo from '../../media/bookers.svg'
import HomeAction from '../actions/HomeAction'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            login: {
                username: '',
                password: '',
                loading: false,
                seePassword: false,
            }
        }
    }

    onChangeLogin = (newObject) => {
        this.setState({ login: { ...this.state.login, ...newObject } })
    }

    onConnect = () => {
        const login = this.state.login
        this.setState({ loading: true })
        HomeAction.login(login.username, login.password, (response) => {
            if (response.token) {
                this.setState({ loading: false })
                this.props.history.replace('/dashboard')
            } else if (response.message) {
                this.setState({ loading: false })
            }
        })
    }

    render() {
        const seePassword = this.state.seePassword
        return (
            <div className='purple-linear-gradient'>
                <div className='background-white'>
                    <Grid className={ 'justify-center' }>
                        <Grid.Column width={ 12 }>
                            <img src={ logo } alt='Bookers' />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input placeholder="Nom d'utilisateur..."
                                label={ { icon: 'user' } }
                                labelPosition='right corner'
                                onChange={ v => this.onChangeLogin({ username: v.target.value }) }
                            />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input action={ { icon: seePassword ? 'eye slash' : 'eye', onClick: () => this.setState({ seePassword: !seePassword }) } }
                                placeholder='Mot de passe...'
                                type={ seePassword ? 'text' : 'password' }
                                onChange={ v => this.onChangeLogin({ password: v.target.value }) } />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Button onClick={ () => this.props.history.replace('/register') } inverted color='violet'>
                                Créer un compte
                            </Button>
                            <Button onClick={ () => this.onConnect() } loading={ this.state.loading } inverted color='violet'>
                                Se connecter
                            </Button>
                        </Grid.Column>
                    </Grid>
                </div>
                <div style={ { height: '100vh' } } />
            </div >
        )
    }
}

export default Login;