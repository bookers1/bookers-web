import React from 'react';
import { Component } from 'react';
import { Loader } from 'semantic-ui-react'
import { connect, } from 'react-redux'
import PropTypes from 'prop-types'
import { BOOKERS_TOKEN, LOGIN } from '../constants/HomeConstants'

class HomeApp extends Component {

    componentDidMount() {
        if (localStorage.getItem(BOOKERS_TOKEN) && localStorage.getItem(LOGIN)) {
            this.props.history.replace('/dashboard')
        } else {
            this.props.history.replace('/login')
        }
    }

    render() {
        return (
            <div>
                <Loader size='massive'>Loading</Loader>
            </div >
        )
    }
}

HomeApp.propTypes = {
    logged: PropTypes.bool,
}
const mapStateToProps = store => ({
    logged: store.HomeReducer.logged,
})

export default connect(mapStateToProps)(HomeApp);