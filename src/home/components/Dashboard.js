import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types'
import { Input, Icon, Card, Button, Image, Label } from 'semantic-ui-react'
import BooksAction from '../../books/actions/BooksAction'
import HomeAction from '../../home/actions/HomeAction'
import DtoBooks from '../../books/dto/DtoBooks'
import { connect, } from 'react-redux'
import NavApp from './NavApp'
import AppStore from '../../store/AppStore'
import DtoUser from '../dto/DtoUser';
import { LOGIN, } from '../constants/HomeConstants'
import BookersLoader from '../../components/BookersLoader';
import defaultLogo from '../../media/logo_size_invert.jpg'

class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            loading: false,
            addingBookLoading: -1
        }
    }

    onLogout = () => {
        HomeAction.logout(() => { this.props.history.replace('/login') })
    }

    setComponentAction = () => {
        const user = this.props.user
        AppStore.dispatch(HomeAction.setAction(
            {
                leftItem: <Input
                    icon={ <Icon name='search' onClick={ () => this.onSearchBooks() } inverted circular link /> }
                    placeholder='Search...'
                    onChange={ v => this.onChangeSearch(v.target.value) }
                />,
                leftItem2: <Label style={ { alignSelf: 'center', margin: '0px' } } onClick={ () => this.props.history.replace('/userProfile') } as='a' content={ user.username } image={ {
                    avatar: true,
                    spaced: 'right',
                    src: 'http://books.google.com/books/content?id=dj8uDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
                } } />,
                rightItem: <Button onClick={ () => this.onLogout() } inverted color='purple'>Se deconnecter</Button>,
            }))
    }

    componentDidMount() {
        const id = localStorage.getItem(LOGIN)
        HomeAction.fetchUser(id, () => this.setComponentAction())
    }

    onChangeSearch = (value) => {
        this.setState({ search: value })
    }

    onSearchBooks = () => {
        const search = this.state.search
        if (search) {
            this.setState({ loading: true })
            BooksAction.searchBooks(search, () => this.setState({ loading: false }))
        }
    }

    onAddBooks = (selfLink, index) => {
        const id = localStorage.getItem(LOGIN)
        this.setState({ addingBookLoading: index })
        BooksAction.addBooks(localStorage.getItem(LOGIN), selfLink, () => {
            HomeAction.fetchUser(id)
            this.setState({ addingBookLoading: -1 })
        })
    }

    onUnAddBooks = (selfLink, index) => {
        const id = localStorage.getItem(LOGIN)
        this.setState({ addingBookLoading: index })
        BooksAction.deleteBooks(localStorage.getItem(LOGIN), selfLink, () => {
            HomeAction.fetchUser(id)
            this.setState({ addingBookLoading: -1 })
        })
    }

    getAddOrUnaddsBoutons = (book, index) => {
        const { user } = this.props
        if (user.usersBooks && user.usersBooks.length && user.usersBooks.find(ub => ub === book.selfLink))
            return (
                <Button loading={ index === this.state.addingBookLoading ? true : false } onClick={ () => this.onUnAddBooks(book.selfLink) } color='green'>
                    { <Icon name='check' /> }
                </Button>
            )
        else {
            return (
                <Button loading={ index === this.state.addingBookLoading ? true : false } onClick={ () => this.onAddBooks(book.selfLink, index) } basic color='green'>
                    { <Icon name='add' /> }
                </Button>
            )
        }
    }

    render() {
        const { books } = this.props
        const loading = this.state.loading
        return (
            <div>
                <NavApp />
                { loading ? <BookersLoader fixed /> : (
                    <div style={ { padding: '2%' } }>
                        <Card.Group itemsPerRow={ 4 }>
                            { books.map((book, i) => {
                                return (
                                    <Card>
                                        <Card.Content>
                                            <Image
                                                floated='right'
                                                size='little'
                                                src={ book.covers.thumbnail ? book.covers.thumbnail : defaultLogo }
                                            />
                                            <Card.Header>{ book.title }</Card.Header>
                                            <Card.Meta>{ book.authors.map(author => author) }</Card.Meta>
                                            <Card.Meta style={ { paddingTop: '2%' } }>{ book.description && book.description.substring(0, 150) + '...' }</Card.Meta>
                                            <Card.Description>
                                                { book.subtitle }
                                            </Card.Description>
                                        </Card.Content>
                                        <Card.Content extra>
                                            <div className='ui two buttons'>
                                                <Button basic color='blue'>
                                                    { <Icon name='info' /> }
                                                </Button>
                                                { this.getAddOrUnaddsBoutons(book, i) }
                                            </div>
                                        </Card.Content>
                                    </Card>
                                )
                            }) }
                        </Card.Group>
                    </div>
                ) }
            </div >
        )
    }
}

Dashboard.propTypes = {
    books: PropTypes.arrayOf(PropTypes.instanceOf(DtoBooks)),
    user: PropTypes.arrayOf(PropTypes.instanceOf(DtoUser)),
}
const mapStateToProps = store => ({
    books: store.BooksReducer.books,
    user: store.HomeReducer.user,
})

export default connect(mapStateToProps)(Dashboard);