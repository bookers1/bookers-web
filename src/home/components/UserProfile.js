import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types'
import { Button, Icon, Label, Card, Grid, Image, Segment, Header } from 'semantic-ui-react'
import HomeAction from '../actions/HomeAction'
import { connect, } from 'react-redux'
import NavApp from './NavApp'
import AppStore from '../../store/AppStore'
import DtoUser from '../dto/DtoUser';
import { LOGIN, } from '../constants/HomeConstants'
import BookersLoader from '../../components/BookersLoader';
import BooksAction from '../../books/actions/BooksAction';
import DtoBooks from '../../books/dto/DtoBooks';
import defaultLogo from '../../media/logo_size_invert.jpg'

class UserProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            loading: false,
            loadingUsersBook: false,
            addingBookLoading: -1
        }
    }

    onLogout = () => {
        HomeAction.logout(() => { this.props.history.replace('/login') })
    }

    setComponentAction = () => {
        AppStore.dispatch(HomeAction.setAction(
            {
                leftItem: <Icon size='large' style={ { margin: '0px' } } onClick={ () => this.props.history.replace('/dashboard') } name='arrow alternate circle left' />,
                leftItem2: <Header as='h4' color='purple'>Mon Profile</Header>,
                rightItem: <Button onClick={ () => this.onLogout() } inverted color='purple'>Se deconnecter</Button>,
            }))
    }

    loadUserBooks = (userBooks) => {
        this.setState({ loading: false, loadingUsersBook: true })
        BooksAction.loadUserBooks(userBooks, () => this.setState({ loadingUsersBook: false }))
    }

    componentDidMount() {
        const id = localStorage.getItem(LOGIN)
        this.setState({ loading: true })
        HomeAction.fetchUser(id, () => this.loadUserBooks(this.props.user.usersBooks))
        this.setComponentAction()
    }

    onAddBooks = (selfLink, index) => {
        const id = localStorage.getItem(LOGIN)
        this.setState({ addingBookLoading: index })
        BooksAction.addBooks(localStorage.getItem(LOGIN), selfLink, () => {
            HomeAction.fetchUser(id)
            this.setState({ addingBookLoading: -1 })
        })
    }

    onUnAddBooks = (selfLink, index) => {
        const id = localStorage.getItem(LOGIN)
        this.setState({ addingBookLoading: index })
        BooksAction.deleteBooks(localStorage.getItem(LOGIN), selfLink, () => {
            HomeAction.fetchUser(id)
            this.setState({ addingBookLoading: -1 })
        })
    }

    getAddOrUnaddsBoutons = (book, index) => {
        const { user } = this.props
        if (user.usersBooks.find(ub => ub === book.selfLink))
            return (
                <Button loading={ index === this.state.addingBookLoading ? true : false } onClick={ () => this.onUnAddBooks(book.selfLink) } color='green'>
                    { <Icon name='check' /> }
                </Button>
            )
        else {
            return (<Button loading={ index === this.state.addingBookLoading ? true : false } onClick={ () => this.onAddBooks(book.selfLink, index) } basic color='green'>
                { <Icon name='add' /> }
            </Button>
            )
        }
    }

    render() {
        const { user, userBooks } = this.props
        const { loading, loadingUsersBook } = this.state
        return (
            <div>
                <NavApp />
                { loading ? <BookersLoader fixed /> : (
                    <div style={ { padding: '2%' } }>
                        <div>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={ 3 }>
                                        <Card>
                                            <Card.Content>
                                                <Image
                                                    floated='right'
                                                    size='large'
                                                    src='http://books.google.com/books/content?id=dj8uDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'
                                                />
                                                <Card.Header>{ user.username }</Card.Header>
                                                <Card.Meta>{ user.firstName + ' ' + user.lastName }</Card.Meta>
                                            </Card.Content>
                                            <Card.Content extra>
                                                <div className='ui two buttons'>
                                                    <Button basic color='red'>
                                                        Supprimer mon compte
                                                    </Button>
                                                </div>
                                            </Card.Content>
                                        </Card>
                                    </Grid.Column>
                                    <Grid.Column width={ 13 }>
                                        <Segment style={ { height: '100%' } } raised>
                                            <Label as='a' color='blue' ribbon>
                                                Description
                                        </Label>
                                            <p style={ { padding: '3%' } }>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially
                                            unchanged. It was popularised in the 1960s with the release
                                            of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                                                publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                            <Label as='a' color='purple' ribbon>
                                                Statistiques
                                        </Label>
                                            <p style={ { padding: '3%' } }>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially
                                            unchanged. It was popularised in the 1960s with the release
                                            of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                                                publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                        </Segment>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </div>
                        <Header as='h2' color='purple'>
                            My Books
                        </Header>
                        <div style={ { padding: '2%' } }>
                            { loadingUsersBook ? <BookersLoader size='huge' /> : (
                                <Card.Group itemsPerRow={ 4 }>
                                    { userBooks.map((book, i) => {
                                        return (
                                            <Card>
                                                <Card.Content>
                                                    <Image
                                                        floated='right'
                                                        size='little'
                                                        src={ book.covers.thumbnail ? book.covers.thumbnail : defaultLogo }
                                                    />
                                                    <Card.Header>{ book.title }</Card.Header>
                                                    <Card.Meta>{ book.authors.map(author => author) }</Card.Meta>
                                                    <Card.Meta style={ { paddingTop: '2%' } }>{ book.description && book.description.substring(0, 150) + '...' }</Card.Meta>
                                                    <Card.Description>
                                                        { book.subtitle }
                                                    </Card.Description>
                                                </Card.Content>
                                                <Card.Content extra>
                                                    <div className='ui two buttons'>
                                                        <Button basic color='blue'>
                                                            Information
                                                    </Button>
                                                        { this.getAddOrUnaddsBoutons(book, i) }

                                                    </div>
                                                </Card.Content>
                                            </Card>
                                        )
                                    }) }
                                </Card.Group>) }
                        </div>
                    </div>
                ) }
            </div >
        )
    }

    componentWillUnmount() {
        AppStore.dispatch(HomeAction.setAction({}))
    }
}

UserProfile.propTypes = {
    user: PropTypes.arrayOf(PropTypes.instanceOf(DtoUser)),
    userBooks: PropTypes.arrayOf(PropTypes.instanceOf(DtoBooks)),
}
const mapStateToProps = store => ({
    user: store.HomeReducer.user,
    userBooks: store.BooksReducer.userBooks,
})

export default connect(mapStateToProps)(UserProfile);