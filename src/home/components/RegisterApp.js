import React from 'react';
import { Component } from 'react';
import { Input, Grid, Button } from 'semantic-ui-react'
import logo from '../../media/bookers.svg'
import HomeAction from '../actions/HomeAction'
import { toast } from 'react-toastify';
import { REGISTRATION_SUCCESSFUL, USERNAME_ALREADY_TAKEN } from '../constants/HomeConstants'
import DtoUser from '../dto/DtoUser'

class RegisterApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                password: '',
                confirmPassword: '',
                loading: false,
            }
        }
    }

    onChangeUser = (newObject) => {
        this.setState({ user: { ...this.state.user, ...newObject } })
    }

    messageErrors = () => {
        const user = this.state.user
        if (!user.firstName || !user.lastName || !user.username || !user.password || !user.confirmPassword) {
            toast.warning("Veuillez remplir tout les champs")
        } else if (user.password !== user.confirmPassword) {
            toast.warning("Les mots de passe ne sont pas identiques")
        } else if (user.password.length < 6) {
            toast.warning("Le mot de passe doit faire plus de 6 charactère")
        }
    }

    formErrors = () => {
        const user = this.state.user
        return (user.password !== user.confirmPassword) ||
            (!user.firstName || !user.lastName || !user.username || !user.password || !user.confirmPassword) ||
            (user.password.length < 6)
    }

    onRegister = () => {
        const user = this.state.user
        if (!this.formErrors()) {
            this.setState({ loading: true })
            HomeAction.register(new DtoUser(user), (response) => {
                if (response.message === REGISTRATION_SUCCESSFUL) {
                    this.setState({ loading: false })
                    this.props.history.replace('/login')
                } else if (response.message === USERNAME_ALREADY_TAKEN) {
                    this.setState({ loading: false })
                }
            })
        } else {
            this.messageErrors()
        }
    }

    render() {
        return (
            <div className='purple-linear-gradient'>
                <div className='background-white'>
                    <Grid className={ 'justify-center' }>
                        <Grid.Column width={ 12 }>
                            <img src={ logo } alt='Bookers' />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input placeholder='Nom...'
                                label={ { icon: 'user' } }
                                labelPosition='right corner'
                                onChange={ v => this.onChangeUser({ firstName: v.target.value }) }
                            />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input placeholder='Prenom...'
                                label={ { icon: 'user' } }
                                labelPosition='right corner'
                                onChange={ v => this.onChangeUser({ lastName: v.target.value }) }
                            />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input placeholder="Nom d'utilisateur..."
                                label={ { icon: 'user' } }
                                labelPosition='right corner'
                                onChange={ v => this.onChangeUser({ username: v.target.value }) }
                            />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input label={ { icon: 'key' } }
                                labelPosition='right corner'
                                placeholder='Mot de passe...'
                                type={ 'password' }
                                onChange={ v => this.onChangeUser({ password: v.target.value }) } />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Input label={ { icon: 'key' } }
                                labelPosition='right corner'
                                placeholder='Confirmer le mot de passe...'
                                type={ 'password' }
                                onChange={ v => this.onChangeUser({ confirmPassword: v.target.value }) } />
                        </Grid.Column>
                        <Grid.Column width={ 12 }>
                            <Button onClick={ () => this.props.history.replace('/login') } inverted color='violet'>
                                Retour
                            </Button>
                            <Button onClick={ () => this.onRegister() } loading={ this.state.loading } inverted color='violet'>
                                Créer son compte
                            </Button>
                        </Grid.Column>
                    </Grid>
                </div>
                <div style={ { height: '100vh' } } />
            </div >
        )
    }
}

export default RegisterApp;