import React from 'react';
import { Component } from 'react';
import { Menu, } from 'semantic-ui-react'
import logo from '../../media/icon.svg'
import { connect, } from 'react-redux'
import PropTypes from 'prop-types'

class NavApp extends Component {

    render() {
        return (
            <div>
                <Menu stackable>
                    <Menu.Item>
                        <img src={ logo } alt='Bookers' />
                    </Menu.Item>
                    { this.props.action.leftItem &&
                        <Menu.Item>
                            { this.props.action.leftItem }
                        </Menu.Item>
                    }
                    { this.props.action.leftItem2 &&
                        <Menu.Item>
                            { this.props.action.leftItem2 }
                        </Menu.Item>
                    }
                    <Menu.Menu position='right'>
                        <Menu.Item>
                            { this.props.action.rightItem }
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
            </div>
        )
    }
}

NavApp.propTypes = {
    action: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
}
const mapStateToProps = store => ({
    action: store.HomeReducer.action,
})

export default connect(mapStateToProps)(NavApp);