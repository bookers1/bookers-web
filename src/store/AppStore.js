import { createStore, combineReducers, applyMiddleware } from 'redux'
import { HomeReducer, store as HomeReducerStore } from '../home/reducers/HomeReducer'
import { BooksReducer, store as BooksReducerStore } from '../books/reducers/BooksReducer'
import { routerReducer, } from 'react-router-redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

const rootReducer = combineReducers({
    HomeReducer,
    BooksReducer,
    routing: routerReducer,
})

const enhancer = applyMiddleware(thunk, logger)

const AppStore = createStore(rootReducer, {
    HomeReducer: HomeReducerStore,
    BooksReducer: BooksReducerStore,
}, enhancer)

export default AppStore
